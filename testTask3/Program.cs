﻿using System;
using System.Diagnostics;

namespace testTask3
{
    class Program
    {

        static Process[] Processes()
        {
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                Console.WriteLine($"Name: {process.ProcessName}, ID: {process.Id}");
            }
            return processes;
        }

        static void Main(string[] args)
        {
            // Список процессов
            Process[] processes = Processes();
            Console.WriteLine("\nEnter the process ID to kill it or enter \"exit\" to leave");
            // Цикл для уничтожения процессов
            for (string input = Console.ReadLine(); input != "exit"; input = Console.ReadLine())
            {
                bool status = false;
                if (Int32.TryParse(input, out int id))
                {
                    foreach (Process process in processes)
                    {
                        if (process.Id == id)
                        {
                            try
                            {
                                // Уничтожение процесса
                                process.Kill();
                                status = true;
                                break;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }

                        }
                    }
                    if (status)
                    {
                        Console.WriteLine("\nList of processes are updated");
                        processes = Processes();
                    }
                } else
                {
                    // Неверный ввод ID
                    Console.WriteLine("Incorrect ID entered");
                    status = true;
                }
                if (!status)
                {
                    // ID не найден
                    Console.WriteLine("ID not found");
                }
                Console.WriteLine("\nEnter the process ID to kill it or enter \"exit\" to leave");
            }

        }
    }
}
